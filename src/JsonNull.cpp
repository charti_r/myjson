#include "JsonNull.hpp"

namespace myjson
{
  JsonNull::JsonNull()
    : JsonValue(JsonValue::Null)
  {

  }

  bool JsonNull::parse(const std::string & str, size_t pos)
  {
    return str.find("null", pos, 4) == 0;
  }
}
