#include "JsonParser.hpp"

namespace myjson
{
  bool JsonParser::parse(const std::string & filename)
  {
    std::ifstream file;
    std::string str;

    file.open(filename.c_str());
    if (file.is_open() == false)
      return false;
    std::getline(file, str, '\0');
    file.close();
    return _json.parse(str);
  }

  JsonParser::operator JsonObject &()
  {
    return _json;
  }

  JsonParser::operator const JsonObject &() const
  {
    return _json;
  }
}
