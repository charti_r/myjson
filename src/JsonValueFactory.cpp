#include "JsonValueFactory.hpp"

namespace myjson
{
  std::unique_ptr<JsonValue> JsonValueFactory::create(const std::string & str, size_t pos)
  {
    if (str[pos] == '{')  return std::unqiue_ptr<JsonValue>(new JsonObject);
    if (str[pos] == '[')  return std::unqiue_ptr<JsonValue>(new JsonArray);
    if (str[pos] == '\"') return std::unqiue_ptr<JsonValue>(new JsonString);

    if (str.find("true", pos, 4) == 0 || str.find("false", pos, 5) == 0)
      return std::unqiue_ptr<JsonValue>(new JsonBool);
    if (str.find("null", pos, 4) == 0)
      return std::unqiue_ptr<JsonValue>(new JsonNull);

    if (str[pos] == '-' || std::isdigit(str[pos])) {
      std::string nb(str.substr(pos, str.find_first_of(" \t\n\0", pos) - pos));

      if (nb.find(".") == std::string::npos)
        return std::unqiue_ptr<JsonValue>(new JsonInt);
      return std::unqiue_ptr<JsonValue>(new JsonFloat);
    }

    return std::unqiue_ptr<JsonValue>(nullptr);
  }
}
