#include "JsonErr.hpp"

namespace myjson
{
  JsonErr::JsonErr(const std::string & error)
    : _error(error)
  {

  }

  const char * JsonErr::what() const noexcept
  {
    return _error.c_str();
  }
}
