#include "JsonString.hpp"

namespace myjson
{
  JsonString::JsonString(const std::string & value)
    : JsonValue(JsonValue::String)
    , _value(value)
  {

  }

  bool JsonString::parse(const std::string &, size_t pos)
  {
    return false;
  }

  JsonString::operator JsonString &()
  {
    return *this;
  }

  JsonString::operator std::string &()
  {
    return _value;
  }

  JsonString::operator const JsonString &() const
  {
    return *this;
  }

  JsonString::operator const std::string &() const
  {
    return _value;
  }
}
