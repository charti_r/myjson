#include "JsonObject.hpp"

namespace myjson
{
  JsonObject::JsonObject()
    : JsonValue(JsonValue::Object)
  {

  }

  bool JsonObject::parse(const std::string & str, size_t pos)
  {
    if (str[pos] != '{')
      return false;

    while (str[pos] != '}') {
      JsonString jstr;
      std::unique_ptr<JsonValue> jval;

      if ((pos = str.find_first_not_of(" \n\t\0", pos)) == std::string::npos)
        return false;
      if (str[pos] == '}')
        return true;

      if (jstr.parse(str, pos) == false)
        return false;

      if ((pos = str.find_first_not_of(" \n\t\0", pos)) == std::string::npos)
        return false;
      if (str[pos] != ':')
        return false;
      if ((pos = str.find_first_not_of(" \n\t\0", pos)) == std::string::npos)
        return false;

      jval = std::move(JsonValueFactory::create(str, pos));
      if (!jval)
        return false;

      if (jval.parse(str, pos) == false)
        return false;
      _values[jstr] = std::move(jval);
    }

    return true;
  }

  JsonValue & JsonObject::operator[](const std::string & value)
  {
    return *(_values[value]);
  }

  const JsonValue & JsonObject::operator[](const std::string & value) const
  {
    return *(_values.at(value));
  }

  JsonObject::operator JsonObject &()
  {
    return *this;
  }

  JsonObject::operator const JsonObject &() const
  {
    return *this;
  }
}
