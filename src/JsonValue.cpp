#include "JsonValue.hpp"

namespace myjson
{
  JsonValue::JsonValue(JsonValue::Type type)
    : _type(type)
  {

  }

  JsonValue::Type JsonValue::getType() const
  {
    return _type;
  }
}
