#include "JsonArray.hpp"

namespace myjson
{
  JsonArray::JsonArray()
    : JsonValue(JsonValue::Array)
  {

  }

  bool JsonArray::parse(const std::string &, size_t pos)
  {
    return false;
  }

  JsonValue & JsonArray::operator[](unsigned int value)
  {
    return *(_values[value]);
  }

  const JsonValue & JsonArray::operator[](unsigned int value) const
  {
    return *(_values[value]);
  }

  JsonArray::operator JsonArray &()
  {
    return *this;
  }

  JsonArray::operator const JsonArray &() const
  {
    return *this;
  }
}
