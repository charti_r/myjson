#ifndef JSON_PARSER_HPP_
# define JSON_PARSER_HPP_

# include <iostream>
# include <fstream>
# include <string>

# include "JsonObject.hpp"

namespace myjson
{
  class JsonParser
  {
  public:
    bool parse(const std::string & filename);

    operator JsonObject &();
    operator const JsonObject &() const;

  private:
    JsonObject _json;
  };
}

#endif // !JSON_PARSER_HPP_
