#ifndef JSON_ERR_HPP_
# define JSON_ERR_HPP_

# include <exception>
# include <string>

namespace myjson
{
  class JsonErr : public std::exception
  {
  public:
    explicit JsonErr(const std::string & error);

    virtual const char * what() const noexcept override;

  private:
    const std::string _error;
  };
}

#endif // !JSON_ERR_HPP_
