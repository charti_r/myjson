#ifndef JSON_VALUE_FACTORY_HPP_
# define JSON_VALUE_FACTORY_HPP_

# include <memory>
# include <string>

# include "JsonValue.hpp"

namespace myjson
{
  class JsonValueFactory
  {
  public:
    static std::unique_ptr<JsonValue> create(const std::string & str, size_t pos = 0);
  };
}

#endif // !JSON_VALUE_FACTORY_HPP_
