#ifndef JSON_VALUE_HPP_
# define JSON_VALUE_HPP_

# include <string>

# include "JsonErr.hpp"

namespace myjson
{
  class JsonObject;
  class JsonArray;
  class JsonString;
  class JsonInt;
  class JsonFloat;
  class JsonBool;
  class JsonNull;

  class JsonValue
  {
  public:
    enum Type
    {
      Object,
      Array,
      String,
      Number,
      Null,
    };

  protected:
    explicit JsonValue(Type type);

  public:
    virtual ~JsonValue() = default;

    virtual bool parse(const std::string &, size_t = 0)
    {
      throw JsonErr("Not Overrided");
    }

    virtual operator JsonObject &()              {throw JsonErr("Not Overrided");}
    virtual operator JsonArray &()               {throw JsonErr("Not Overrided");}
    virtual operator JsonString &()              {throw JsonErr("Not Overrided");}
    virtual operator std::string &()             {throw JsonErr("Not Overrided");}
    virtual operator JsonInt &()                 {throw JsonErr("Not Overrided");}
    virtual operator int &()                     {throw JsonErr("Not Overrided");}
    virtual operator JsonFloat &()               {throw JsonErr("Not Overrided");}
    virtual operator float &()                   {throw JsonErr("Not Overrided");}
    virtual operator JsonBool &()                {throw JsonErr("Not Overrided");}
    virtual operator bool &()                    {throw JsonErr("Not Overrided");}
    virtual operator JsonNull &()                {throw JsonErr("Not Overrided");}

    virtual operator const JsonObject &()  const {throw JsonErr("Not Overrided");}
    virtual operator const JsonArray &()   const {throw JsonErr("Not Overrided");}
    virtual operator const JsonString &()  const {throw JsonErr("Not Overrided");}
    virtual operator const std::string &() const {throw JsonErr("Not Overrided");}
    virtual operator const JsonInt &()     const {throw JsonErr("Not Overrided");}
    virtual operator const int &()         const {throw JsonErr("Not Overrided");}
    virtual operator const JsonFloat &()   const {throw JsonErr("Not Overrided");}
    virtual operator const float &()       const {throw JsonErr("Not Overrided");}
    virtual operator const JsonBool &()    const {throw JsonErr("Not Overrided");}
    virtual operator const bool &()        const {throw JsonErr("Not Overrided");}
    virtual operator const JsonNull &()    const {throw JsonErr("Not Overrided");}

    Type getType() const;

  protected:
    const Type _type;
  };
}

#endif // !JSON_VALUE_HPP_
