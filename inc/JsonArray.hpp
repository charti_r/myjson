#ifndef JSON_ARRAY_HPP_
# define JSON_ARRAY_HPP_

# include <memory>
# include <string>
# include <vector>

# include "JsonValue.hpp"

namespace myjson
{
  class JsonArray : public JsonValue
  {
  public:
    JsonArray();

    virtual bool parse(const std::string & str, size_t pos = 0) override;

    JsonValue & operator[](unsigned int value);
    const JsonValue & operator[](unsigned int value) const;

    virtual operator JsonArray &() override;
    virtual operator const JsonArray &() const override;

  private:
    std::vector<std::unique_ptr<JsonValue>> _values;
  };
}

#endif // !JSON_ARRAY_HPP_
