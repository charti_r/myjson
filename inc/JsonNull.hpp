#ifndef JSON_NULL_HPP_
# define JSON_NULL_HPP_

# include <string>

# include "JsonValue.hpp"

namespace myjson
{
  class JsonNull : public JsonValue
  {
  public:
    JsonNull();

    virtual bool parse(const std::string & str, size_t pos = 0);
  };
}

#endif // !JSON_NULL_HPP_
