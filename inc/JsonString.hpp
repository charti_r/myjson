#ifndef JSON_STRING_HPP_
# define JSON_STRING_HPP_

# include <string>

# include "JsonValue.hpp"

namespace myjson
{
  class JsonString : public JsonValue
  {
  public:
    JsonString(const std::string & value = std::string());

    virtual bool parse(const std::string & str, size_t pos = 0) override;

    virtual operator JsonString &() override;
    virtual operator std::string &() override;

    virtual operator const JsonString &() const override;
    virtual operator const std::string &() const override;

  private:
    std::string _value;
  };
}

#endif // !JSON_STRING_HPP_
