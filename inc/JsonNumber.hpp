#ifndef JSON_NUMBER_HPP_
# define JSON_NUMBER_HPP_

# include <string>

# include "JsonValue.hpp"

namespace myjson
{
  template <typename T>
  class JsonNumber : public JsonValue
  {
  public:
    JsonNumber(const T & value)
      : JsonValue(JsonValue::Number)
      , _value(value)
    {

    }

    virtual bool parse(const std::string & str, size_t pos = 0) override
    {
      return false;
    }

    virtual operator JsonNumber<T> &() override             {return *this;}
    virtual operator T &() override                         {return _value;}

    virtual operator const JsonNumber<T> &() const override {return *this;}
    virtual operator const T &() const override             {return _value;}

  private:
    T _value;
  };
}

typedef JsonInt   JsonNumber<int>;
typedef JsonFloat JsonNumber<float>;
typedef JsonBool  JsonNumber<bool>;

#endif // !JSON_NUMBER_HPP_
