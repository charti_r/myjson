#ifndef JSON_OBJECT_HPP_
# define JSON_OBJECT_HPP_

# include <memory>
# include <string>
# include <map>

# include "JsonValue.hpp"

namespace myjson
{
  class JsonObject : public JsonValue
  {
  public:
    JsonObject();

    virtual bool parse(const std::string & str, size_t pos = 0) override;

    JsonValue & operator[](const std::string & value);
    const JsonValue & operator[](const std::string & value) const;

    virtual operator JsonObject &() override;
    virtual operator const JsonObject &() const override;

  private:
    std::map<std::string, std::unique_ptr<JsonValue>> _values;
  };
}

#endif // !JSON_OBJECT_HPP_
