default: all

NAME 			= myjson
NAME_STC	= lib$(NAME).a
NAME_DYN	= lib$(NAME).so

SRCDIR		= ./src
INCDIR		= ./inc
OBJDIR		= ./obj

SRC				:= \
					$(SRCDIR)/JsonParser.cpp \
					$(SRCDIR)/JsonErr.cpp \
					$(SRCDIR)/JsonValue.cpp \
					$(SRCDIR)/JsonObject.cpp \
					$(SRCDIR)/JsonArray.cpp \
					$(SRCDIR)/JsonString.cpp \
					$(SRCDIR)/JsonNull.cpp

OBJ				:= $(SRC:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

RM				= rm -f
CC				= g++

CPPFLAGS	+= -Wall -Wextra -pedantic -o3 -std=c++14 -fPIC
CPPFLAGS	+= -I $(INCDIR)

all: $(NAME_STC) $(NAME_DYN)

$(OBJ): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CC) $(CPPFLAGS) -c $< -o $@

$(NAME_STC): $(OBJ)
	ar rc $(NAME_STC) $(OBJ)
	ranlib $(NAME_STC)

$(NAME_DYN): $(OBJ)
	$(CC) $(CPPFLAGS) $(OBJ) -o $(NAME_DYN)

clean:
	$(RM) $(OBJ)
	$(RM) $(SRC:.cpp=.cpp~) $(SRC:$(SRCDIR)/%.cpp=$(INCDIR)/%.hpp~) $(OBJ:.o=.o~)

fclean: clean
	$(RM) $(NAME_STC) $(NAME_DYN)

re: fclean all

.PHONY		= all $(NAME_STC) $(NAME_DYN) clean fclean re
