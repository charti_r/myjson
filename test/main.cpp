#include <string>

#include "../inc/JsonParser.hpp"

int main()
{
  myjson::JsonParser parser;

  if (parser.parse("file.json") == false)
    return 1;
  return 0;
}
